/*
 *
 * former enzian/src/components/elements/block--map-list-toggle/map-list-toggle.js
 * 
 */

(function($) {
  'use strict';
  Drupal.behaviors.h4c_viewtoggle = {
    attach: function (context, settings) {

      // Set Map List Toggle Links according to context
      if (jQuery('#block-h4cviewtoggle').length) {

        var locationSearch = window.location.search.replace('?', '&'); // Use as non-first parameters

        // Set correct link active
        jQuery.each(['/map', '/organisations','/events'], function( index, value ) {
          if (window.location.pathname.indexOf(value) >= 0) {
            jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="'+value+'"]').addClass('is-active');
          }
        });

        // Replace link urls with current url parameters with some extra conditions
        // On /map and /organisations pages find /map and /organisations links
        if ( (((window.location.pathname.indexOf('/') >= 0) && (jQuery('#block-h4cviewtoggle').length))) || (window.location.pathname.indexOf('/map') >= 0) || (window.location.pathname.indexOf('/organisations') >= 0) ) {
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/map"]').attr('href', '/map' + window.location.search);
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/organisations"]').attr('href', '/organisations' + window.location.search);
        }

        // On /map pages find /organisation links when showing events
        if ( (((window.location.pathname.indexOf('/') >= 0) && (jQuery('#block-h4cviewtoggle').length))) && (window.location.search.indexOf('event') >= 0) ) {
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/organisations"]').attr('href', '/events' + window.location.search);
        }

        // On /organisations pages without organisation url parameter find /map links 
        if ( (window.location.pathname.indexOf('/organisations') >= 0) && (window.location.search.indexOf('content-type%3Aorganisation') == -1) ) {
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/map"]').attr('href', '/map?f[x]=content-type%3Aorganisation' + locationSearch);
        }

        // On /events pages without event url parameter find /map links 
        if ( (window.location.pathname.indexOf('/events') >= 0) && (window.location.search.indexOf('content-type%3Aevent') == -1) ) {
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/map"]').attr('href', '/map?f[x]=content-type%3Aevent' + locationSearch);
        }

        // On /events pages with event url parameter find /events and /map links
        if ( (window.location.pathname.indexOf('/events') >= 0) && (window.location.search.indexOf('content-type%3Aevent') >= 0) ) {
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/events"]').attr('href', '/events' + window.location.search);
          jQuery('#block-h4cviewtoggle .h4c-toggle-box a[href*="/map"]').attr('href', '/map' + window.location.search);
        }
      }

  }};
})(jQuery);

/*
 *
 * former enzian/src/components/elements/block--facet-filter/facets-filter.js
 * 
 */

/*
 * Behaviour of facet filter:
 *  - Check sub items of checked main items and toggle visibility
 *  - Toggle sub item visbility if main item icons get clicked
 *  - Toggle facet filter visbility when block title gets clicked
 */

(function($) {
  'use strict';
  /**
   * Expand sub items if parents or sub items are seleted
   */
  function setFacetExpandedState() {
    $('.facets-widget-checkbox input[type="checkbox"]').each(function (e) {
      var facetItem = $(this).closest('li.facet-item');
      if ($(this).is(':checked') || facetItem.find("ul input:checked").length) {
        facetItem.addClass('facet-item--selected facet-item--expanded');
      }
      else {
        facetItem.removeClass('facet-item--expanded');
      }
    });
  }

  $(document).ready(function(){
    // Call in $(document).ready because in Drupal.behaviors.enzian_facets_filters, 
    // it is called to early - before filter elements are loaded!
    // Calling it in Drupal.behaviors.facetsViewsAjax = { attach: function () { ... } } 
    // results in the same problem.
    setFacetExpandedState();
  });

  // Expand sub items if parents or sub items are seleted
  setFacetExpandedState();

  // open / close sub items
  $('.facets-widget-checkbox li.facet-item').has('ul').addClass('facet-item--has-sub-items');
  $('.facets-widget-checkbox li.facet-item').on('click', function (e) {
    if (e.offsetX > $(this).width()) {
      // Only animate after loading (i.e. on click)
      $(this).addClass('facet-item--animate');
      if (!$(this).hasClass('facet-item--expanded')) {
        // Open sub items
        $(this).children('div').slideDown(400);
        $(this).addClass('facet-item--expanded');
      }
      else {
        // Close sub items
        $(this).children('div').slideUp(400);
        $(this).removeClass('facet-item--expanded');
      }
    }
  });

  // Find out if block is expanded (desktop) or collapsed (mobile) and set class
  $('.h4c-filter__facetwrapper').each(function(){
    if ($(this).children('div').first().is(':visible')) {
      $(this).addClass('block--expanded');
    }
  });

  $('.h4c-filter__facetwrapper h2').on('click', function () {
    var block = $(this).parent();
    block.addClass('block--animate');
    if (block.children('div').first().is(':hidden')) {
      block.children('div').slideDown();
      block.addClass('block--expanded');
    }
    else {
      block.children('div').slideUp();
      block.removeClass('block--expanded');
    }
  });

  // Toggle filter bar on small screens 

  Drupal.behaviors.h4c_filter_toggle = {
    attach: function (context, settings) {

    $('.js-h4c-filter-toggle').once('h4c_filter_toggle').click(function () {
      $('.h4c-filter__wrapper').toggleClass('js-h4c-show'); 
    });

  }};

})(jQuery);


