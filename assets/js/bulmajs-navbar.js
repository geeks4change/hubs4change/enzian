/**
 * @file
 * Bulma scripts for theme.
 * 
 * Enzian: Adopted from bulma module.
 *
 */
(function ($) {
  'use strict';

  Drupal.behaviors.bulma = {
    attach: function (context) {
      var $toggle = $('.navbar-burger').once('bulma');
      if ($toggle.length) {
        var $menu = $('.navbar-menu');

        $toggle.click(function () {
          $(this).toggleClass('is-active');
          $menu.toggleClass('is-active');
        });
      }
    }
  };

})(jQuery);
