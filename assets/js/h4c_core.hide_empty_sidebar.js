(function ($, Drupal) {
  Drupal.behaviors.h4c_hide_empty_sidebar = {
    attach: function (context) {
      
      'use strict';

      /* Visually hide empty sidebar to not block underlying elements */
      /* Related: https://gitlab.com/geeks4change/hubs4change/h4c/issues/327 / https://gitlab.com/geeks4change/hubs4change/enzian/issues/81 */
      /* TODO: Remove this JS and add proper template based solution */
      $('aside', context).once('hide-empty-sidebar').each(function () {
        if ($(this).children().length == 0){
          $(this).css('height', '0');
        }
      });

    }
  };
})(jQuery, Drupal);

