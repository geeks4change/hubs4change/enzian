(function ($, Drupal) {
  'use strict';
  // Set correct dimensions/aspect ratio for oembed according to https://www.drupal.org/project/drupal/issues/3061134
  Drupal.behaviors.h4c_core_media_scale_oembed = {
    attach: function (context) {

      $('.media-oembed-content', context).once('scale-oembed').each(function () {
          var parentWidth = $(this).parent().width();
          $(this).width(parentWidth).height(parentWidth/1.7);
      });

  }};
  // Limit images taken up most part of the window height in content area
  Drupal.behaviors.h4c_core_media_scale_large_images = {
    attach: function (context) {
      $('.h4c-content img', context).once('scale-large-images').on('load',function() {
        var imgHeight = $(this).height();
        var windowHeight = $(window).height();

        if(imgHeight > windowHeight * 0.8) { 
          $(this).css({
            width: "50%",
            height: "auto"
          });
        }
      });

  }};
  // Make embedded media gallery full width
  Drupal.behaviors.h4c_core_media_scale_gallery = {
    attach: function (context) {

    $('[id^=slick-media-h4c-media-gallery].slick--skin--fullwidth', context).once('scale-media-gallery').each(function () {
      // Do not apply for nested embedded entities, e.g. for galleries in FAQ where the FAQ itself is embedded
      if (!jQuery('.embedded-entity .embedded-entity').has(this).length) {

        var embedLeft = $(this).offset().left;
        var embedWidthNew = $(window).width();

        $(this).width(embedWidthNew).css({
          'margin-left': - embedLeft,
          'max-width': 'unset',
          'z-index': '502',
        });
        $(".slick__arrow", this).css({
          'z-index': '1000',
        });
      }
    });

  }};
})(jQuery, Drupal);