(function ($) {
  'use strict';
  Drupal.behaviors.h4c_filter_toggle = {
    attach: function (context, settings) {

    // Toggle active class on parent div to show / hide preview display
    // and to highlight media display
    $('.js-h4c-filter-toggle').once('h4c_filter_toggle').click(function () {
      $('.h4c-filter__facetwrapper').toggleClass('js-h4c-show'); 
    });


  }};

})(jQuery);

