/*eslint strict: ["error", "global"]*/
'use strict';

//=======================================================
// Include gulp
//=======================================================
var gulp = require('gulp');

//=======================================================
// Include Our Plugins
//=======================================================
var concat      = require('gulp-concat');
var order       = require('gulp-order');
var sync        = require('browser-sync');

// Export our tasks.
module.exports = {

  // Concat all CSS into a master bundle.
  css: function() {
    return gulp.src([
      './assets/css/*.css'
    ])
      // Reorder the files so global is first.
      // If you need to get fancier with the order here's an example:
      // .pipe(order([
      //   'assets/css/global.css',
      //   'src/components/**/*.css',
      //   'assets/css/btn.css',
      //   'assets/css/form-item.css',
      //   'assets/css/form-float-label.css',
      //   'assets/css/*.css'
      // ], { base: './' }))
      .pipe(order([
        'assets/css/global.css',
        'assets/css/*.css'
      ], { base: './' }))
      .pipe(concat('all.css'))
      .pipe(gulp.dest('./assets/css'))
      .pipe(sync.stream());
  }
};
