COPIED OVER FROM H4C CORE

# H4C Short infos item list
Introduced in https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/426

## Description
- Technical admins can create a list of items per view display
- Some items contain multiple (inline) fields, see below
- Icons are hardcoded in template files
- Used in organisation/event full, organistation popup/preview, event teaser/popup

## List items provided with icon

*Location*
- location_reference (use location micro, link title if reusable -> done via template)
- h4c_location_more_info

*Wheelchair*
- h4c_location_wheelchair

*Opening hours*
- h4c_opening_hours
- h4c_opening_hours_text

*Phone(s)*
- h4c_phones

*Website*
- h4c_website

*Email*
- h4c_email

*Organisations*
- h4c_organisations

## Templates
### h4c_core
* field__field_location_reference
* field_group_html_element__group_event_date => move to h4c_event
* field_group_html_element__group_h4c_costs
* field_group_html_element__group_h4c_email
* field_group_html_element__group_h4c_location_reference
* field_group_html_element__group_h4c_location_wheelchair
* field_group_html_element__group_h4c_opening_hours
* field_group_html_element__group_h4c_organisations
* field_group_html_element__group_h4c_phone
* field_group_html_element__group_h4c_website

### h4c_location
* node__location__micro
* field__field_location_geofield

## CSS 
- Provided in `h4c_core.module.css`

## JS
- No JS

