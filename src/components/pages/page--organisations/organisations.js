(function ($) {
  'use strict';
  Drupal.behaviors.h4c_organisations = {
    attach: function (context, settings) {

    // H4C Preview
    // Toggle active class on parent div to show / hide preview display
    // and to highlight media display
    $('.js-h4c-toggle .js-h4c-intial').once('h4c_preview').click(function () {
      $(this).parent().toggleClass('js-active').siblings().removeClass('js-active');
    });

    // Close button function
    $('.js-h4c-preview .field--name-node-title').click(function () {
      $(this).parents('.js-active').removeClass('js-active');
    });


  }};

})(jQuery);

