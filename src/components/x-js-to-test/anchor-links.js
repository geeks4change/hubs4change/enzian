(function ($, Drupal){
  $(window).on('load',function() {

    if(jQuery('#toolbar-administration').length) {
      var offsetHeight = 189;
    } else {
      var offsetHeight = 115;
    }
    /* Not needed? 
    $('a[href^="#"]').click(function () {
        $('html, body').animate({
            scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top - offsetHeight
        }, 300);

        return false;
    });
    */
    if ($('.node-form').length) {
        $("a[href^='#']").click(function (event) {
          event.preventDefault();
          jQuery('html, body').animate({
              scrollTop: $($.attr(this, 'href')).offset().top - offsetHeight
          }, 300);
        });
    }
  });
})(jQuery, Drupal);