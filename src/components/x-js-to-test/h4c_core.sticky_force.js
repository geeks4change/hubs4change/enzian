(function ($, Drupal) {
  'use strict';

  // The h4c-sticky--force element is used to make elements position sticky throughout the whole page (in cases the parent element is too small to let elements stick). Another h4c-sticky--secondary element's top value is adjusted to be displayed below the forced element. 
  // You can use Element 1: h4c-sticky--force / Element 2: h4c-sticky--secondary 
  // ATM You cannot use Element 1: h4c-sticky alone / Element 2: h4c-sticky--secondary
  // You need h4c-sticky on both elements

  $('.h4c-sticky--force').once('sticky-force').each(function () {

    // Views block is always shown even if empty, so we only want to do something if there is actually a link present
    // QUICKFIX: Do not check because we might not know which element to check for
    //if ($(this).find("li").length > 0){

    var elementHeight = $(this).height();

    // Move menu block to dialog-off-canvas-main-canvas so that position: sticky makes the menu stick throughout the whole page (parent containers height dictates where element sticks)
    $(this).css('height', elementHeight).prependTo('.dialog-off-canvas-main-canvas');

    // Update top value of secondary block if both are present
    if ( ($('.h4c-sticky--secondary').length) && ($('.h4c-sticky--force').length) ) {
      var secondaryTop = parseInt($('.h4c-sticky--secondary').css('top'));
      $('.h4c-sticky--secondary').css('top',secondaryTop + parseInt($(this).height()) + 'px' );
    }

    // Correct anchor links height if both are present 
    if ( ($('.h4c-anchor').length) && ($('.h4c-sticky--force').length) ) {
      $('.h4c-anchor').each(function () {
        var anchorTop = parseInt($(this).css('top'));
        $(this).css('top', anchorTop - parseInt(elementHeight) + 'px');
      });
    }

    // }

  });

})(jQuery, Drupal);
